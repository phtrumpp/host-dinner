package de.academy.jee.hostDinner.model;


public class PostModel {

    public PostModel() {
    }

    public PostModel(String content, String title) {
        this.content = content;
        this.title = title;
    }

    private String content;
    private String title;
    private Long id;

    @Override
    public String toString() {
        return "PostModel{" +
                "content='" + content + '\'' +
                ", title='" + title + '\'' +
                ", id=" + id +
                '}';
    }

    public String getContent() {
        return content;
    }

    public String getTitle() {
        return title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
