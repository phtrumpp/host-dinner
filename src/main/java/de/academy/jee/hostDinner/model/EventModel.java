package de.academy.jee.hostDinner.model;

import de.academy.jee.hostDinner.entities.Ingridient;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventModel {

    private String dishName;
    private Date date;
    private String time;
    private String description;
    private Long id;
    private Long userId;

    private List<Ingridient> ingredients;


    public EventModel() {
    }

    public EventModel(String dishName,
                      Date date,
                      String time) {
        this.dishName = dishName;
        this.date = date;
        this.time = time;

    }

    public EventModel(String dishName,
                      Date date,
                      String time,
                      String description,
                      ArrayList<Ingridient> ingredients) {
        this.dishName = dishName;
        this.date = date;
        this.time = time;
        this.description = description;
        this.ingredients = ingredients;

    }


    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Ingridient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<Ingridient> ingredients) {
        this.ingredients = ingredients;
    }
}
