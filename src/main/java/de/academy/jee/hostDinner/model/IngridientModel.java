package de.academy.jee.hostDinner.model;

import de.academy.jee.hostDinner.entities.User;

public class IngridientModel {

    private String ingridientName;
    private Long id;
    private User gast;
    private String einkaufsliste;

    public String getEinkaufsliste() {
        return einkaufsliste;
    }

    public void setEinkaufsliste(String einkaufsliste) {
        this.einkaufsliste = einkaufsliste;
    }

    public IngridientModel() {
    }

    public IngridientModel(String ingridientName) {
        this.ingridientName = ingridientName;

    }


    public void setIngridientName(String ingridientName) {
        this.ingridientName = ingridientName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}