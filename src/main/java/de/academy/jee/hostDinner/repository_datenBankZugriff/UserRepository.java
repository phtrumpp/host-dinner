package de.academy.jee.hostDinner.repository_datenBankZugriff;

import de.academy.jee.hostDinner.entities.User;
import de.academy.jee.hostDinner.model.EventModel;

import java.util.List;

public class UserRepository extends Repository<User> {

    public UserRepository() {
        super(User.class);
    }

    /**
     * Creates a Query which looks for the username (unique) in the database
     *
     * @param username takes the username from the login input
     * @return Returns the required entity by the username
     */
    public User lookUp(String username) {

        User user = (User) entityManager.createQuery("SELECT u FROM User u WHERE u.userNickName = "
                + "'" + username + "'").getSingleResult();

        return user;
    }

    @Override
    public void safeToDB(User safeEntity) {
        super.safeToDB(safeEntity);
    }

    @Override
    public List<User> readAll() {
        return super.readAll();
    }

    @Override
    public List<User> readEntytyByEvent(EventModel event) {
        return super.readEntytyByEvent(event);
    }

    @Override
    public User readEntityByID(Long commentId) {
        return super.readEntityByID(commentId);
    }

    @Override
    public void removeFromDB(User removeEntity) {
        super.removeFromDB(removeEntity);
    }
}
