package de.academy.jee.hostDinner.repository_datenBankZugriff;

import de.academy.jee.hostDinner.entities.User;
import de.academy.jee.hostDinner.services.MD5Hash;

import java.io.Serializable;


public class LoginRepository extends Repository<User> implements Serializable {


    public LoginRepository() {
        super(User.class);
    }


    public boolean loginControl(String username, String password) {


        try {
            String hashedPassword = MD5Hash.generateHashCode(password);
            Object l = entityManager.createQuery
                    ("SELECT l FROM User l WHERE l.userNickName = :username AND l.userPassword = :password").setParameter("username", username)
                    .setParameter("password", hashedPassword).getSingleResult();
            return l != null;
        } catch (Exception e) {
            return false;
        }

        // if (l!=null) {
        // return true;
        // }
    }
}
