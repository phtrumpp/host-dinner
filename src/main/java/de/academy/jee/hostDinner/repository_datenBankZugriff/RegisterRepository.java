package de.academy.jee.hostDinner.repository_datenBankZugriff;

import de.academy.jee.hostDinner.entities.User;

import javax.ejb.Stateless;


@Stateless
public class RegisterRepository extends Repository<User> {


    public RegisterRepository() {
        super(User.class);
    }

    @Override
    public void safeToDB(User safeEntity) {
        super.safeToDB(safeEntity);
    }


    /**
     * Creates a Query which looks for the username (unique) in the database
     *
     * @param username takes the username from the login input
     * @return Returns the required entity by the username
     */
    public User lookUp(String username) {

        User user = (User) entityManager.createQuery("SELECT u FROM User u WHERE u.userNickName = "
                + "'" + username + "'").getSingleResult();

        return user;
    }
}
