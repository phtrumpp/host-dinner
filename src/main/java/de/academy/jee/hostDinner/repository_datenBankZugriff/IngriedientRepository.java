package de.academy.jee.hostDinner.repository_datenBankZugriff;

import de.academy.jee.hostDinner.entities.Ingridient;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;

@Stateless
public class IngriedientRepository extends Repository<Ingridient> {

    private EntityManager entityManager;

    public IngriedientRepository() {
        super(Ingridient.class);
    }

    public void safeToDB(Ingridient safeEntity) {
        super.safeToDB(safeEntity);
    }


}
