package de.academy.jee.hostDinner.repository_datenBankZugriff;

import de.academy.jee.hostDinner.entities.Event;

import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class EventRepository extends Repository<Event> {


    public EventRepository() {
        super(Event.class);
    }

    @Override
    public void safeToDB(Event safeEntity) {
        super.safeToDB(safeEntity);
    }

    @Override
    public List<Event> readAll() {
        return super.readAll();
    }

    @Override
    public Event readEntityByID(Long eventId) {
        return super.readEntityByID(eventId);
    }
}
