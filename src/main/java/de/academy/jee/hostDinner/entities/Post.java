package de.academy.jee.hostDinner.entities;


import javax.persistence.*;

@Entity
public class Post {


    public Post() {
    }

    public Post(String title, String content) {
        this.title = title;
        this.content = content;
    }

    @Id
    @GeneratedValue
    private Long id;


    private String content;

    @Column(nullable = false)
    private String title;


    /**
     * Getter und Setter
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }


    public String getContent() {
        return content;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
