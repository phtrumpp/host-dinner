package de.academy.jee.hostDinner.entities;


import javax.persistence.*;
import java.util.List;

@Entity
public class User {

    public User() {
    }

    public User(String userFirstName, String userLastName, String userGender, String userNickName,
                String userEmail, String userPassword, String userAdress) {
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userGender = userGender;
        this.userNickName = userNickName;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.userAdress = userAdress;
    }

    @Id
    @GeneratedValue
    private Long id;


    @OneToMany
    @JoinColumn(name = "user_id")
    private List<Event> eventList;


    private String userFirstName;


    private String userLastName;

    private String userGender;


    @Column(unique = true)
    private String userNickName;

    public String getUserNickName() {
        return userNickName;
    }

    private String userEmail;


    private String userPassword;


    private String userAdress;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

}
