package de.academy.jee.hostDinner.entities;


import javax.persistence.*;

import java.util.ArrayList;

import java.util.Date;
import java.util.List;


@Entity
public class Event {


    @Id
    @GeneratedValue
    private Long id;
    private Date date;
    private String time;
    @Column(length = 500)
    private String description;
    private String dishName;


    @OneToMany
    @JoinColumn(name = "event_id")
    private List<Ingridient> ingridients = new ArrayList<>();

    public Event() {
    }


    public Event(String dishName, Date date, String time, String description) {
        this.date = date;
        this.time = time;
        this.dishName = dishName;
        this.description = description;
        //ToDo --- K&P --- //
        // this.userId = loginBB.giveMeName(loginBB.getUsername()).getId();

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }


    public String getTime() {
        return time;
    }


    public String getDescription() {
        return description;
    }


    public String getDishName() {
        return dishName;
    }

    public List<Ingridient> getIngridients() {
        return ingridients;
    }

}
