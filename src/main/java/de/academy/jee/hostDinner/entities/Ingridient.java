package de.academy.jee.hostDinner.entities;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Ingridient {

    @Id
    @GeneratedValue
    private Long id;
    String ingridientName;
    private String gast;

    public Ingridient() {
    }

    public Ingridient(String ingridientName, Long id, User gast) {
    }

    public Ingridient(String ingridientName) {
        this.ingridientName = ingridientName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIngridientName() {
        return ingridientName;
    }

    public void setIngridientName(String ingridientName) {
        this.ingridientName = ingridientName;
    }


}
