package de.academy.jee.hostDinner.services;

import de.academy.jee.hostDinner.entities.Ingridient;
import de.academy.jee.hostDinner.model.EventModel;
import de.academy.jee.hostDinner.model.IngridientModel;
import de.academy.jee.hostDinner.repository_datenBankZugriff.IngriedientRepository;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;


@Stateless
public class IngredientServiceBean {

    @EJB
    private IngriedientRepository ingriedientRepository;

    public IngredientServiceBean() {
    }


    // Map all IngredientsEntities for event
    public List<IngridientModel> readAllIngredients(EventModel event) {
        List<Ingridient> ingridients = ingriedientRepository.readEntytyByEvent(event);

        List<IngridientModel> ingridientModels = new ArrayList<>();
        for (Ingridient ingridient : ingridients) {
            IngridientModel ingridientModel = new IngridientModel();
            ingridientModel.setIngridientName(ingridient.getIngridientName());
            ingridientModel.setId(ingridient.getId());
            ingridientModels.add(ingridientModel);
        }
        return ingridientModels;
    }


}
