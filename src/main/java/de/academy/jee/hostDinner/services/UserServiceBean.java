package de.academy.jee.hostDinner.services;


import de.academy.jee.hostDinner.entities.User;

import de.academy.jee.hostDinner.model.UserModel;
import de.academy.jee.hostDinner.repository_datenBankZugriff.UserRepository;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class UserServiceBean {

    @EJB
    UserRepository userRepository;

    public List<UserModel> readAllUser() {

        List<User> userList = userRepository.readAll();
        List<UserModel> userModels = new ArrayList<>();
        for (User user : userList) {
            UserModel UserModel = new UserModel();
            // User Model bekommt die UserId
            UserModel.setId(user.getId());

            userModels.add(UserModel);


        }
        return userModels;

    }


}
