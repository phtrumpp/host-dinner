package de.academy.jee.hostDinner.services;

import de.academy.jee.hostDinner.entities.User;
import de.academy.jee.hostDinner.model.UserModel;
import de.academy.jee.hostDinner.repository_datenBankZugriff.RegisterRepository;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.io.Serializable;

@Stateless
public class RegisterServiceBean implements Serializable {

    @Inject
    private RegisterRepository registerRepository;

    public RegisterServiceBean() {
    }

    @Transactional
    public void createAndSafeUser(UserModel userModel) {

        // --- Generieren ein Hash PW für die DB
        String userModelHashedPW = MD5Hash.generateHashCode(userModel.getUserPassword());
        User safeUser = new User(userModel.getUserFirstName(),
                userModel.getUserLastName(),
                userModel.getUserGender(),
                userModel.getUserNickName(),
                userModel.getUserEmail(), userModelHashedPW,
                userModel.getUserAdress());
        registerRepository.safeToDB(safeUser);
    }

    public User getUserName(String userName) {
        return registerRepository.lookUp(userName);
    }

}
