package de.academy.jee.hostDinner.services;

import de.academy.jee.hostDinner.entities.Event;
import de.academy.jee.hostDinner.entities.Ingridient;
import de.academy.jee.hostDinner.entities.User;
import de.academy.jee.hostDinner.model.EventModel;
import de.academy.jee.hostDinner.model.IngridientModel;

import de.academy.jee.hostDinner.repository_datenBankZugriff.EventRepository;
import de.academy.jee.hostDinner.repository_datenBankZugriff.IngriedientRepository;


import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Stateless
public class EventServiceBean {

    @Inject
    private EventRepository eventRepository;
    @Inject
    private IngriedientRepository ingriedientRepository;

    //ToDo User an Event binden
//    @Inject
//    private UserRepository userRepository;


    public EventServiceBean() {
    }


        // Methode Macht aus einem String mit Zutaten einzelne Ingriediens
        // und speichert diese in der Datendank
        public void createAndSafeEvetsAndIngr(EventModel eventModel, IngridientModel ingridientModel){

        // Safe Event in DB
            Event newEvent = new Event(eventModel.getDishName(),eventModel.getDate(),
                    eventModel.getTime(),eventModel.getDescription());
            eventRepository.safeToDB(newEvent);

            String[] ingredientList = ingridientModel.getEinkaufsliste().split(",");
            ArrayList<String> ingriedientsNames = new ArrayList<>(Arrays.asList(ingredientList));

            for (String name: ingriedientsNames){

                Ingridient neuIngredient = new Ingridient();
                neuIngredient.setIngridientName(name);
                ingriedientRepository.safeToDB(neuIngredient);

                newEvent.getIngridients().add(neuIngredient);
                eventRepository.safeToDB(newEvent);

            }


    }


    public List<EventModel> readAllEvents() {

        List<Event> events = eventRepository.readAll();
        List<EventModel> eventModels = new ArrayList<>();
        for (Event event : events) {
            EventModel eventModel = new EventModel();



            eventModel.setDishName(event.getDishName());
            eventModel.setDate(event.getDate());
            eventModel.setTime(event.getTime());
            eventModel.setDescription(event.getDescription());
            eventModel.setId(event.getId());
            eventModel.setIngredients(event.getIngridients());

            eventModels.add(eventModel);
        }
        return eventModels;

    }

    public EventModel readByID(Long id) {
        EventModel eventModelToSchow = new EventModel();
        Event event = eventRepository.readEntityByID(id);
        eventModelToSchow.setDishName(event.getDishName());
        eventModelToSchow.setDate(event.getDate());
        eventModelToSchow.setTime(event.getTime());
        eventModelToSchow.setDescription(event.getDescription());
        eventModelToSchow.setId(event.getId());
        eventModelToSchow.setIngredients(event.getIngridients());
        System.out.println("Serviesebien");

        return eventModelToSchow;
    }

}
