package de.academy.jee.hostDinner.beans;


import de.academy.jee.hostDinner.model.EventModel;
import de.academy.jee.hostDinner.model.IngridientModel;
import de.academy.jee.hostDinner.services.EventServiceBean;
import de.academy.jee.hostDinner.services.IngredientServiceBean;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import java.util.List;

@Named
@RequestScoped
public class EventController {

    @Inject
    private EventServiceBean eventServiceBean;

    @Inject
    private IngredientServiceBean ingredientServiceBean;


    private EventModel eventModel;
    private IngridientModel ingridientModel;

    //ToDo User an Event binden
//    private UserModel userModel = new UserModel();


    public EventController() {
        this.eventModel = new EventModel();
        this.ingridientModel = new IngridientModel();
    }

    public void createEvent() {
        eventServiceBean.createAndSafeEvetsAndIngr(eventModel, ingridientModel);

    }
    public EventModel readEventToShow(Long id){
        return  eventServiceBean.readByID(id);

    }

    public List<EventModel> readAllEvents() {
        return eventServiceBean.readAllEvents();
    }


    public List<IngridientModel> readAllIngredients() {
        return ingredientServiceBean.readAllIngredients(eventModel);

    }


    public EventModel getEventModel() {
        return eventModel;
    }

    public IngridientModel getIngridientModel() {
        return ingridientModel;
    }

}
