package de.academy.jee.hostDinner.beans;

import de.academy.jee.hostDinner.model.UserModel;
import de.academy.jee.hostDinner.services.RegisterServiceBean;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@RequestScoped
public class RegisterBB implements Serializable {

    @Inject
    private RegisterServiceBean registerServiceBean;

    private UserModel userModel;

    public UserModel getUserModel() {
        return userModel;
    }

    private String userFirstName;
    private String userLastName;
    private String userGender;
    private String userNickName;
    private String userEmail;
    private String userPassword;
    private String userAdress;

    @PostConstruct
    private void init() {
        this.userModel = new UserModel(
                this.userFirstName, this.userLastName, this.userGender, this.userNickName,
                this.userEmail, this.userPassword, this.userAdress);
    }


    public void createUser() {
        registerServiceBean.createAndSafeUser(userModel);
    }


}
