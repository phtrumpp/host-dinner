package de.academy.jee.hostDinner.beans;


import de.academy.jee.hostDinner.entities.User;
import de.academy.jee.hostDinner.model.UserModel;
import de.academy.jee.hostDinner.repository_datenBankZugriff.LoginRepository;
import de.academy.jee.hostDinner.services.RegisterServiceBean;
import org.primefaces.PrimeFaces;


import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;
import java.io.Serializable;

@Named("login")
@SessionScoped
public class LoginBB implements Serializable {

    @Inject
    private RegisterServiceBean registerServiceBean;

    private String username;
    private String password;


    @Inject
    private LoginRepository loginRepository;


    /**
     * Checks if username and password are correct. If they are, it redirects to
     * start.xhtml(pinnwall) if it is not, it shows a message that username
     * or password are incorrect.
     */
    public String loginControl() {
        if (loginRepository.loginControl(username, password)) {
            return "start.xhtml?faces-redirect=true";
        }
        PrimeFaces.current().ajax().update("growl");
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                "Error", "Username or Password is incorrect."));
        return "";
    }

    public User giveMeName(String username) {

        try {
            //TODo User an Event binden -- Exception kam von hier
//            userModel.setSessionedUserName(registerServiceBean.getUserName(username).getUserNickName());
            return registerServiceBean.getUserName(username);
        } catch (NoResultException ignore) {
            return null;
        }

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
