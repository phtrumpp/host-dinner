package de.academy.jee.hostDinner.services;


import de.academy.jee.hostDinner.entities.Event;
import de.academy.jee.hostDinner.entities.Ingridient;
import de.academy.jee.hostDinner.model.IngridientModel;


import de.academy.jee.hostDinner.repository_datenBankZugriff.EventRepository;
import de.academy.jee.hostDinner.repository_datenBankZugriff.IngriedientRepository;


import org.junit.Test;

import javax.persistence.EntityManager;

import java.util.ArrayList;
import java.util.Arrays;



import static org.junit.Assert.*;


public class EventTest {
    private EntityManager entityManager;
    private Event event;
    private Ingridient ingridient;

    // Beans injezieren funktioniert nicht in Testklassen, da der Container Kontext nicht existiert
//    @Inject
//    PostRepository postRepository;


//     Methode Macht aus einem String mit Zutaten einzelne Ingriediens
//     und speichert diese in der Datendank
    @Test
    public void testCreateAndSafeEventsAndIngr() {

        IngriedientRepository ingriedientRepository = new IngriedientRepository();
        EventRepository eventRepository = new EventRepository();

        // Safe in DB
        Event eventTestIngridient = new Event();

        IngridientModel ingridientModelTest = new IngridientModel();
        ingridientModelTest.setEinkaufsliste("TestGurke,Testkartoffel,TestIrgendwas,Testbla");

        String[] ingredientList = ingridientModelTest.getEinkaufsliste().split(",");
        ArrayList<String> ingriedientsNames = new ArrayList<>(Arrays.asList(ingredientList));

        for (String name : ingriedientsNames) {

            Ingridient neuIngredient = new Ingridient();
            neuIngredient.setIngridientName(name);
            ingriedientRepository.safeToDB(neuIngredient);

            eventTestIngridient.getIngridients().add(neuIngredient);
            eventRepository.safeToDB(eventTestIngridient);
            Event testEvent = eventRepository.readEntityByID(eventTestIngridient.getId());

            assertEquals(testEvent.getIngridients(), eventTestIngridient.getIngridients());


        }


    }



}
