package de.academy.jee.hostDinner.services;


import de.academy.jee.hostDinner.entities.Ingridient;
import de.academy.jee.hostDinner.model.EventModel;
import de.academy.jee.hostDinner.model.IngridientModel;

import org.junit.Test;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class IngridientTest {


    @Test
    public void testReadAllIngredients() {

        EventModel event = new EventModel();

        // DummyLists
        List<Ingridient> ingridients = Arrays.asList(new Ingridient(), new Ingridient(), new Ingridient());
        List<IngridientModel> ingridientModels = new ArrayList<>();

        // mapping
        for (Ingridient ingridient : ingridients) {
            IngridientModel ingridientModel = new IngridientModel();
            ingridientModel.setIngridientName(ingridient.getIngridientName());
            ingridientModel.setId(ingridient.getId());
            ingridientModels.add(ingridientModel);
        }
        assertEquals(ingridientModels.size(), ingridients.size());

    }
}
