package de.academy.jee.hostDinner.services;
import de.academy.jee.hostDinner.entities.User;
import de.academy.jee.hostDinner.model.UserModel;
import de.academy.jee.hostDinner.repository_datenBankZugriff.RegisterRepository;
import org.junit.Test;


import static org.junit.Assert.assertEquals;


public class RegisterTest {

    RegisterRepository registerRepository = new RegisterRepository();

    @Test
    public void testCreateAndSafeUser() {


        User safeUser = new User("Junit","Junit",
                "male","unitTest","@gmail.com",
                "passwort","straße");

        registerRepository.safeToDB(safeUser);

        User user = registerRepository.lookUp("unitTest");

        assertEquals(user.getUserNickName(), safeUser.getUserNickName());
    }


}
